#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* num[] = {"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18", "19","20","21","22","23","24","25","26","27","28","29","30","31","32"};

int main() {
char command[32][300];

for (int j=0; j<32; j++) {
	strcpy(command[j], "tools/cnf2bdd/pc2bdd_static benchmark/grade_45/domain_decomp/grade_45_domain_");
	strcat(command[j], num[j]);
	strcat(command[j], "_opt.dimacs benchmark/grade_45/domain_decomp/grade_45_domain_");
	strcat(command[j], num[j]);
	strcat(command[j], ".bdd");
}

#pragma omp parallel for
	for (int i=0; i<32; i++) {
		system(command[i]);
	}
	return 0;
}