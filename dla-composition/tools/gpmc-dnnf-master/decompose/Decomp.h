#ifndef DECOMP_H
#define DECOMP_H

#if defined(_MSC_VER) || defined(__MINGW32__)
#include <time.h>

static inline double cpuTime(void) {
	return (double)clock() / CLOCKS_PER_SEC;
}
#else
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

static inline double cpuTime(void) {
    struct rusage ru;
    getrusage(RUSAGE_SELF, &ru);
    return (double)ru.ru_utime.tv_sec + (double)ru.ru_utime.tv_usec / 1000000;
}
#endif

#include <deque>
#include <fstream>
#include <iostream>
#include <set>
#include <vector>

#include <gmpxx.h>

typedef enum {
	AND_NODE, OR_NODE, LIT_NODE, TOP_NODE, BOTTOM_NODE
} NodeType;

char* readFile(std::ifstream& ifs);
static inline void skipWhitespace(char** in);
static inline int parseInt(char** in);
static inline void skipLine(char** in);
std::vector<int> parseIntLine(char** in);
static inline bool eagerMatch(char* a, const char* b) {
	while(*a == *b) {
		a++;
		b++;
	}
	return *b == 0;
}
static inline int parseInt(char** in) {
    int     val = 0;
    int    _neg = 0;
    skipWhitespace(in);
    if(**in == '-') {
    	_neg = 1;
    	(*in)++;
    } else if (**in == '+') {
    	(*in)++;
    }
    if (**in < '0' || **in > '9') {
    	std::cerr << "PARSE ERROR! Unexpected char: " << **in << std::endl;
    	exit(1);
    }
    while (**in >= '0' && **in <= '9') {
    	val = val*10 + (**in - '0');
        (*in)++;
    }
    return _neg ? -val : val;
}
static inline void skipWhitespace(char** in) {
    while (**in == 9  || **in == 32) {
    	(*in)++;
    }
}
static inline void skipLine(char** in) {
    while(**in != 0) {
    	if (**in == '\n') {
    		(*in)++;
    		return;
    	}
        (*in)++;
    }
}

class Node {
	NodeType type;
	int literal;
	std::vector<Node*> children;
	mpz_class numModel;

public:
	Node(NodeType type) :
		type(type),
		literal(0) {
		this->numModel = (type == AND_NODE || type == TOP_NODE) ? 1 : 0;
	}

	Node(NodeType type, int lit, int num = 1) :
		type(LIT_NODE),
		literal(lit),
		numModel(num) {
	}

	int getLiteral() {
		return this->literal;
	}
	mpz_class getNumModel() {
		return this->numModel;
	}

	void addChild(Node* node);
	void getModel(mpz_class count, std::deque<int>& model);
	mpz_class modifyModel(std::set<int>& filter);
};

class ModelIterator;

class Models {
	Node* node;
	std::set<int> filter;
	mpz_class numModel, beginIt, endIt;

public:
	typedef ModelIterator iterator;
	Models() :
		node(NULL),
		filter(),
		numModel(0),
		beginIt(0),
		endIt(0) {
	}

	~Models() {
		delete this->node;
	}

	mpz_class getNumModel() {
		return this->numModel;
	}

	void setNode(Node* node) {
		this->node = node;
	}
	Node* getNode() {
		return this->node;
	}
	void setFilter(std::set<int> filter);
	void setNumModel(mpz_class n);

	bool isFiltered();

	ModelIterator begin();
	ModelIterator end();
};

class ModelIterator {

	Node*& node;
	std::set<int>& filter;
	mpz_class& numModel;
	mpz_class& begin;
	mpz_class& end;

	mpz_class index;

public:
	ModelIterator(Node*& node, std::set<int>& filter, mpz_class& numModel, mpz_class& begin, mpz_class& end, mpz_class index) :
		node(node),
		filter(filter),
		numModel(numModel),
		begin(begin),
		end(end),
		index(index) {
	}
	ModelIterator(const ModelIterator& it) :
		node(it.node),
		filter(it.filter),
		numModel(it.numModel),
		begin(it.begin),
		end(it.end),
		index(it.index) {
	}

	void satisfy(int i);
	bool isSatisfy();

	ModelIterator& operator++() {
		this->index++;
		return *this;
	}
	ModelIterator operator++(int) {
		ModelIterator ret = *this;

		this->index++;
		return ret;
	}
	std::deque<int> operator*() {
		std::deque<int> model;
		this->node->getModel(this->index, model);
		return model;
	}

	bool operator==(const ModelIterator& it) {
		return this->index == it.index;
	}
	bool operator!=(const ModelIterator& it) {
		return this->index != it.index;
	}

	mpz_class getIndex() {
		return this->index;
	}
};

class Decomp {

	int numNode, numEdge, numVar;
	std::set<int> filter;

public:
	Models models;


	Decomp() :
		numNode(0),
		numEdge(0),
		numVar(0),
		models() {
	}

	int getNumNode() {
		return this->numNode;
	}
	int getNumEdge() {
		return this->numEdge;
	}
	int getNumVar() {
		return this->numVar;
	}
	mpz_class getNumModel() {
		return this->models.getNumModel();
	}

	void setNumNode(int v) {
		this->numNode = v;
	}
	void setNumEdge(int e) {
		this->numEdge = e;
	}
	void setNumVar(int n) {
		this->numVar = n;
	}
	void setFilter(std::set<int> filter) {
		// this->models.setFilter(filter);
		this->filter = filter;
	}
	bool isFiltered() {
		return this->models.isFiltered();
	}

	void setNode(std::ifstream& ifs);
	void decompose(char* filename, double start, long int timeout);
};

#endif
