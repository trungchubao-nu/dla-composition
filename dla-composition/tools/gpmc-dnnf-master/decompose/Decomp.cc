#include <algorithm>

#include "Decomp.h"

char* readFile(std::ifstream& ifs) {
	int begin = static_cast<int>(ifs.tellg());
	ifs.seekg(0, ifs.end);
	int end = static_cast<int>(ifs.tellg());
	int size = end - begin;
	ifs.clear();
	ifs.seekg(0, ifs.beg);
	char* ret = new char[size + 1];
	ret[size] = 0;
	ifs.read(ret, size);
	return ret;
}

std::vector<int> parseIntLine(char** in) {
	std::vector<int> ret;
	while(**in != '\n') {
		ret.push_back(parseInt(in));
	}
	return ret;
}

void Node::addChild(Node* node) {
	this->children.push_back(node);
	if(this->type == AND_NODE) {
		this->numModel *= node->getNumModel();
	} else { // this->type == OR_NODE
		this->numModel += node->getNumModel();
	}
}

void Node::getModel(mpz_class count, std::deque<int>& model) {
	std::deque<int>::iterator it;
	mpz_class tmp;

	switch(this->type) {
	case AND_NODE:
		for(int i = 0; i < this->children.size(); i++) {
			tmp = this->children[i]->getNumModel();
			// this->children[i]->getModel(count - count / tmp * tmp, model);
			this->children[i]->getModel(count % tmp, model);
			count /= tmp;
		}
		return;
	case OR_NODE:
		tmp = this->children[0]->getNumModel();
		if(count < tmp) {
			this->children[0]->getModel(count, model);
		} else {
			this->children[1]->getModel(count - tmp, model);
		}
		return;
	case LIT_NODE:
		for(it = model.begin(); it != model.end(); it++) {
			if(this->literal == (*it)) {
				return;
			} else if(std::abs(this->literal) < std::abs(*it)) {
				break;
			}
		}
		model.insert(it, this->literal);
		return;
	case TOP_NODE:
	case BOTTOM_NODE:
	default:
		return;
	}
}

mpz_class Node::modifyModel(std::set<int>& filter) {
	mpz_class tmp;
	switch(this->type) {
	case AND_NODE:
		tmp = 1;
		for(int i = 0; i < this->children.size(); i++) {
			tmp *= this->children[i]->modifyModel(filter);
		}
		this->numModel = tmp;
		break;
	case OR_NODE:
		this->numModel = this->children[0]->modifyModel(filter) + this->children[1]->modifyModel(filter);
		break;
	case LIT_NODE:
		this->numModel = std::find(filter.begin(), filter.end(), -this->literal) == filter.end() ? 1 : 0;
		break;
	case TOP_NODE:
		this->numModel = 1;
		break;
	case BOTTOM_NODE:
		this->numModel = 0;
		break;
	default:
		break;
	}
	return this->numModel;
}

void Models::setFilter(std::set<int> filter) {
	this->filter = filter;
	this->setNumModel(this->node->modifyModel(filter));
}

void Models::setNumModel(mpz_class n) {
	this->numModel = n;
	this->endIt = n;
}

bool Models::isFiltered() {
	return this->filter.size() != 0;
}

ModelIterator Models::begin() {
	ModelIterator ret(this->node, this->filter, this->numModel, this->beginIt, this->endIt, this->beginIt);
	return ret;
}

ModelIterator Models::end() {
	ModelIterator ret(this->node, this->filter, this->numModel, this->beginIt, this->endIt, this->endIt);
	return ret;
}

void Decomp::setNode(std::ifstream& ifs) {
    char* text = readFile(ifs);
    char* in = text;
	int count = 0;
	std::string str;
	std::vector<int> bf;
	std::vector<Node*> node;

    skipWhitespace(&in);
    if(!eagerMatch(in, "nnf")) {
		std::cerr << "PARSE ERROR!" << std::endl;
		std::exit(1);
    }
    in += 3;
    bf = parseIntLine(&in);
    if(bf.size() != 3) {
		std::cerr << "PARSE ERROR!" << std::endl;
		std::exit(1);
    }
    this->setNumNode(bf[0]);
    this->setNumEdge(bf[1]);
    this->setNumVar(bf[2]);

    in++;

	while(true) {
		Node* tmp;
        skipWhitespace(&in);
        if(*in == 0) {
        	break;
        } else if(*in == 'A') {
        	in++;
        	bf = parseIntLine(&in);
        	if(bf[0] == 0) {
        		tmp = new Node(TOP_NODE);
        	} else {
        		tmp = new Node(AND_NODE);
            	for(int i = 1; i < bf.size(); i++) {
            		tmp->addChild(node[bf[i]]);
            	}
        	}
            node.push_back(tmp);
        } else if(*in == 'O') {
        	in++;
        	bf = parseIntLine(&in);
        	if(bf[0] == 0 && bf[1] == 0) {
        		tmp = new Node(BOTTOM_NODE);
        	} else if(bf[1] == 2) {
        		tmp = new Node(OR_NODE);
            	tmp->addChild(node[bf[2]]);
            	tmp->addChild(node[bf[3]]);
        	} else {
        		std::cerr << "PARSE ERROR!" << std::endl;
        		std::exit(1);
        	}
            node.push_back(tmp);
        } else if(*in == 'L') {
        	in++;
        	int lit = parseInt(&in);
		if(filter.find(-lit) != filter.cend())
        		tmp = new Node(LIT_NODE, lit, 0);
		else
			tmp = new Node(LIT_NODE, lit);
            node.push_back(tmp);
        }
        skipLine(&in);
	}
	this->models.setNode(node.back());
	this->models.setNumModel(node.back()->getNumModel());

	delete[] text;
	return;
}

void Decomp::decompose(char* filename, double start = 0, long int timeout = -1) {
	std::ofstream ofs(filename);
	if(!ofs) return;
/*
	for(Models::iterator it = this->models.begin(); it != this->models.end(); it++) {
		for(int i = 0; i < (*it).size(); i++) {
			ofs << (*it)[i] << " ";
		}
		ofs << "0\n";
	}
*/
       std::deque<int> model;
       for(mpz_class idx = 0; idx < this->models.getNumModel(); idx++){
               this->models.getNode()->getModel(idx, model);
               for(int i = 0; i < model.size(); i++) {
                       ofs << model[i] << " ";
               }
               ofs << "0\n";
               model.clear();

               if (((cpuTime() - start) * 1000 > timeout) && timeout > 0) {
                  ofs.close();
                  return;
               }
       }

	ofs.close();

	return;
}

