#include <iomanip>

#include "Decomp.h"

std::set<int> parseFilter(std::ifstream& ifs) {
    char* text = readFile(ifs);
    char* in = text;
    std::set<int> ret;
    std::vector<int> tmp = parseIntLine(&in);

    for(int i = 0; i < tmp.size(); i++) {
    	if(tmp[i] != 0) {
    		ret.insert(tmp[i]);
    	}
    }

	delete[] text;
	return ret;
}

void printUsage(char* str) {
	std::cerr << "Usage:\t" << str << " <d-DNNF-file> [<output-file>] [-f <filter-file>] [-t <timeout>]" << std::endl;
}

int main(int argc, char** argv) {
	char* nnfFile = NULL;
	char* outFile = NULL;
	char* fltFile = NULL;
	long int timeout = -1;
	Decomp dec;

	if(argc < 2) {
		printUsage(argv[0]);
		return 1;
	}

	for(int i = 1; i < argc; i++) {
		if (eagerMatch(argv[i], "-t\0")) {
			i++;
			if (i == argc) {
				printUsage(argv[0]);
				return 1;
			}
			timeout = std::stol(argv[i]);
		}
		else if(eagerMatch(argv[i], "-f\0")) {
			i++;
			if(i == argc) {
				printUsage(argv[0]);
				return 1;
			}
			fltFile = argv[i];
		} else if(nnfFile == NULL) {
			nnfFile = argv[i];
		} else if(outFile == NULL) {
			outFile = argv[i];
		} else {
			printUsage(argv[0]);
			return 1;
		}
	}
	
	std::set<int> filter;
	if(fltFile != NULL) {
		std::ifstream fltIfs(fltFile);
		if(fltIfs.fail()) {
			std::cerr << "ERROR! Could not open file: " << fltFile << std::endl;
			return 1;
		}
		filter = parseFilter(fltIfs);
		fltIfs.close();
	}

	std::ifstream nnfIfs(nnfFile);
	if(nnfIfs.fail()) {
		std::cerr << "ERROR! Could not open file: " << nnfFile << std::endl;
		return 1;
	}
	double beforeCount = cpuTime();
	dec.setFilter(filter);
	dec.setNode(nnfIfs);
	double afterCount = cpuTime();
	nnfIfs.close();

	std::cout << "Number of nodes      : " << std::setw(12) << std::left << dec.getNumNode() << std::endl;
	std::cout << "Number of edges      : " << std::setw(12) << std::left << dec.getNumEdge() << std::endl;
	std::cout << "Number of proj vars  : " << std::setw(12) << std::left << dec.getNumVar() << std::endl;
	gmp_printf("#Projected Models    = %Zd\n", dec.getNumModel());
	std::cout << "CPU time (count)     : " <<  afterCount - beforeCount << " s" << std::endl;
	fflush(stdout);

	double beforeDecomp = cpuTime();
	dec.decompose(outFile, beforeCount, timeout);
	double afterDecomp = cpuTime();

	std::cout << "CPU time (decompose) : " <<  afterDecomp - beforeDecomp << " s" << std::endl;
	fflush(stdout);

	return 0;
}
