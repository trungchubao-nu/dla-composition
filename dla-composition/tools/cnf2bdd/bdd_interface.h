/** \file     bdd_interface.h
 *  \brief    Functions to access BDD libraries.
 *  \author   Takahisa Toda
 */
#ifndef BDD_INTERFACE_H
#define BDD_INTERFACE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <limits.h>

#include "my_def.h"

#if defined(REDUCTION) // added
/*---------------------------------------------------------------------------*/
/* CUDD PACKAGE                                                  */
/*---------------------------------------------------------------------------*/
#include "util.h"
#include "cudd.h"
#include "cuddInt.h"

#define BDD_PACKAGE "CU Decision Diagram Package Release 2.5.0"
#define BDD_NULL  NULL
#define BDD_MAXITEMVAL  CUDD_MAXINDEX //!< maximum value that a BDD library can handle.

typedef DdNode *bddp; //!< pointer to a BDD node

/* Common Operations */
static inline int bdd_init(DdManager** dd_mgr, itemval maxval, uintmax_t n)
{
  if(*dd_mgr == NULL) {
    *dd_mgr = Cudd_Init((DdHalfWord)maxval, (DdHalfWord)maxval, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, UINTMAX_C(1) << 34);
    //dd_mgr = Cudd_Init((DdHalfWord)maxval, (DdHalfWord)maxval, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
    ENSURE_TRUE_MSG(*dd_mgr != NULL, "BDD manager initialization failed.");
    Cudd_DisableGarbageCollection(*dd_mgr);// disable GC since we never do dereference of nodes.
#ifdef MISC_LOG
    if(Cudd_GarbageCollectionEnabled(*dd_mgr)) printf("GC\tenabled\n");
    else                                      printf("GC\tdisabled\n");
#endif /*MISC_LOG*/

    return ST_SUCCESS;
  } else {
    ENSURE_TRUE_WARN(false, "BDD manager already initialized.");
    return ST_FAILURE;
  }
}

static inline int bdd_quit(DdManager** dd_mgr)
{
  if(*dd_mgr != NULL) {
    Cudd_Quit(*dd_mgr);
    *dd_mgr = NULL;
    return ST_SUCCESS;
  } else {
    ENSURE_TRUE_WARN(false, "BDD manager does not exist.");
    return ST_FAILURE;
  }
}

/* BDD Operations*/
static inline itemval bdd_itemval(DdManager** dd_mgr, bddp f)
{
  assert(*dd_mgr != NULL);
  assert(f      != BDD_NULL);

  bddp t = Cudd_Regular(f);
  return (itemval)Cudd_NodeReadIndex(t);
}

static inline bddp bdd_top(DdManager** dd_mgr)
{
  assert(*dd_mgr != NULL);

  return Cudd_ReadOne(*dd_mgr);
}

static inline bddp bdd_bot(DdManager** dd_mgr)
{
  assert(*dd_mgr != NULL);

  return Cudd_ReadLogicZero(*dd_mgr);
}

static inline int bdd_isconst(DdManager** dd_mgr, bddp f)
{
  assert(f != BDD_NULL);

  return (f==bdd_top(dd_mgr) || f==bdd_bot(dd_mgr));
}

static inline uintmax_t bdd_size(DdManager** dd_mgr, bddp f)
{
  assert(*dd_mgr != NULL);
  assert(f      != BDD_NULL);

  return (uintmax_t)Cudd_DagSize(f);
}

static inline bddp bdd_hi(DdManager** dd_mgr, bddp f)
{
  assert(*dd_mgr != NULL);
  assert(f      != BDD_NULL);
  assert(!bdd_isconst(dd_mgr, f));

  bddp t = Cudd_Regular(f);
  if (Cudd_IsComplement(f)) return Cudd_Not(cuddT(t));
  else                      return cuddT(t);
}

static inline bddp bdd_lo(DdManager** dd_mgr, bddp f)
{
  assert(*dd_mgr != NULL);
  assert(f      != BDD_NULL);
  assert(!bdd_isconst(dd_mgr, f));

  bddp t = Cudd_Regular(f);
  if (Cudd_IsComplement(f)) return Cudd_Not(cuddE(t));
  else                      return cuddE(t);
}


static inline bddp bdd_and(DdManager** dd_mgr, bddp f, bddp g)
{
  assert(*dd_mgr != NULL);
  assert(f != BDD_NULL && g != BDD_NULL);

  return Cudd_bddAnd(*dd_mgr, f, g);
}

static inline bddp bdd_node(DdManager** dd_mgr, itemval i, bddp lo, bddp hi)
{
  assert(*dd_mgr != NULL);
  assert(!bdd_isconst(dd_mgr, hi)? i < bdd_itemval(dd_mgr, hi): true);
  assert(!bdd_isconst(dd_mgr, lo)? i < bdd_itemval(dd_mgr, lo): true);

  bddp f;
  if(lo == hi) {
    f = hi;
  } else {
    if (Cudd_IsComplement(hi)) {
      f = cuddUniqueInter(*dd_mgr,(int)i,Cudd_Not(hi),Cudd_Not(lo));
      ENSURE_TRUE_MSG(f != BDD_NULL, "BDD operation failed");
      f = Cudd_Not(f);
    } else {
      f = cuddUniqueInter(*dd_mgr,(int)i, hi, lo);
      ENSURE_TRUE_MSG(f != BDD_NULL, "BDD operation failed");
    }
  }
  return f;
}

#endif /*REDUCTION*/

#endif /*BDD_INTERFACE_H*/
