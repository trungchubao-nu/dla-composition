#include <iomanip>

#include "Decomp.h"

std::set<int> parseFilter(std::ifstream& ifs) {
    char* text = readFile(ifs);
    char* in = text;

    std::vector<int> tmp = parseIntLine(&in);
    std::set<int> ret;
    for(int i = 0; i < tmp.size(); i++) {
    	if(tmp[i] != 0) {
    		ret.insert(tmp[i]);
    	}
    }

	delete[] text;
	return ret;
}

std::vector<int> parseTable(std::ifstream& ifs) {
	int size = 0, tmpSize = 0;
    char* text = readFile(ifs);
    char* in = text;
    std::vector<int> lits, table, ret;

    while(true) {
    	skipWhitespace(&in);
    	if (*in == 0)
    		break;
    	else if(eagerMatch(in, "cr")) {
    		int loc = 0, parsed_lit;
    		in += 2;
    		if(tmpSize == 0) {
    			int max = 0;
    			std::vector<int> tmp;
    			while(true) {
    				parsed_lit = std::abs(parseInt(&in));
    				if(parsed_lit == 0) {
    					break;
    				}
    				tmp.push_back(parsed_lit);
    				max = max < parsed_lit ? parsed_lit : max;
    			}
    			size = tmp.size();
    			for(int i = 0; i < max; i++) {
    				table.push_back(-1);
    				ret.push_back(-1);
    			}
    			for(int i = 0; i < size; i++) {
    				table[tmp[i] - 1] = loc;
    				loc++;
    			}
    			for(int i = 0; i < max; i++) {
    				if(table[i] == -1) {
    					table[i] = loc;
    					loc++;
    				}
    				ret[table[i]] = i + 1;
    			}
    		} else {
    			int counter = 0;
    			for(int i = 0; i < tmpSize; i++) {
    				table[i] = -1;
    			}
    			while(true) {
    				parsed_lit = std::abs(parseInt(&in));
    				if(parsed_lit == 0) {
    					break;
    				}
    				table[parsed_lit - 1] = loc;
    				loc++;
    				counter++;
    			}
    			size = counter;
    			for(int i = 0; i < tmpSize; i++) {
    				if(table[i] == -1) {
    					table[i] = loc;
    					loc++;
    				}
    				ret[table[i]] = i + 1;
    			}
    		}
    	} else if (*in == 'c') {
    		skipLine(&in);
    	} else if (*in == 'p') {
    		int i;
    		in += 5;
    		tmpSize = parseInt(&in);
    		for(i = table.size(); i < tmpSize; i++) {
    			table.push_back(i);
    			ret.push_back(i);
    		}
    		if(size == 0) {
    			size = tmpSize;
    		}
    		skipLine(&in);
    	} else{
    		skipLine(&in);
    	}
    }
    ret.resize(size);
	delete[] text;
	return ret;
}

void printUsage(char* str) {
	std::cerr << "Usage:\t" << str << " input-bdd-file input-cnf-file output-file [filter-file]" << std::endl;
}

int main(int argc, char** argv) {
	Decomp dec;
	char* bddFile = NULL;
	char* cnfFile = NULL;
	char* outFile = NULL;
	char* fltFile = NULL;
    long int timeout = -1; // timeout in milisec, added by Trung Chu Bao 14/4/2019

	if(argc < 3) {
		printUsage(argv[0]);
		return 1;
	}

	for(int i = 1; i < argc; i++) {
        if(eagerMatch(argv[i], "-t\0")) {   // timeout: added by Trung Chu Bao 14/4/2019
            i++;
            if (i == argc) {
                printUsage(argv[0]);
                return 1;
            }
            timeout = std::stol(argv[i]);
        }
		else if(eagerMatch(argv[i], "-f\0")) {
			i++;
			if(i == argc) {
				printUsage(argv[0]);
				return 1;
			}
			fltFile = argv[i];
		} else if(bddFile == NULL) {
			bddFile = argv[i];
		} else if(cnfFile == NULL) {
			cnfFile = argv[i];
		} else if(outFile == NULL) {
			outFile = argv[i];
		} else {
			printUsage(argv[0]);
			return 1;
		}
	}

	std::ifstream cnfIfs(cnfFile);
	if(cnfIfs.fail()) {
		std::cerr << "ERROR! Could not open file: " << cnfFile << std::endl;
		return 1;
	}
	std::vector<int> table = parseTable(cnfIfs);
	cnfIfs.close();

	std::ifstream bddIfs(bddFile);
	if(bddIfs.fail()) {
		std::cerr << "ERROR! Could not open file: " << bddFile << std::endl;
		return 1;
	}
	bddIfs.close();
	dec.setNode(bddFile, table);

	if(fltFile != NULL) {
		std::ifstream fltIfs(fltFile);
		if(fltIfs.fail()) {
			std::cerr << "ERROR! Could not open file: " << fltFile << std::endl;
			return 1;
		}
		std::set<int> filter = parseFilter(fltIfs);
		fltIfs.close();
        dec.setFilter(filter);
	}

	std::cout << "Input-bdd            : " << argv[1] << std::endl;
	std::cout << "Variables            : " << std::setw(12) << std::left << dec.getNumVar() << std::endl;
	std::cout << "|bdd|                : " << std::setw(12) << std::left << dec.getBddSize() << std::endl;
	dec.printlnNumModel("#Projected Models    : ");

	double beforeDecomp = cpuTime();
	mpz_class numModel = dec.decompose(outFile, beforeDecomp, timeout);
	double afterDecomp = cpuTime();

	if(dec.isFiltered()) {
		std::cout << "  #Filtered Models   : " << numModel << std::endl;
	}

	std::cout <<     "CPU time (decompose) : " << std::defaultfloat << afterDecomp - beforeDecomp << " sec" << std::endl;

	return 0;
}
