#ifndef DECOMP_H
#define DECOMP_H

#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <ctime>

#include <gmpxx.h>

#include "cudd.h"
#include "cuddInt.h"

#if defined(_MSC_VER) || defined(__MINGW32__)
#include <time.h>

static inline double cpuTime(void) {
	return (double)clock() / CLOCKS_PER_SEC;
}
#else
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

static inline double cpuTime(void) {
    struct rusage ru;
    getrusage(RUSAGE_SELF, &ru);
    return (double)ru.ru_utime.tv_sec + (double)ru.ru_utime.tv_usec / 1000000;
}
#endif

typedef unsigned long long uint64;

char* readFile(std::ifstream& ifs);
static inline void skipLine(char** in);std::vector<int> parseIntLine(char** in);
bool isFind(std::set<int> filter, int n);
static inline void skipWhitespace(char** in) {
    while (**in == 9  || **in == 32) {
    	(*in)++;
    }
}
static inline int parseInt(char** in) {
    int     val = 0;
    int    _neg = 0;
    skipWhitespace(in);
    if(**in == '-') {
    	_neg = 1;
    	(*in)++;
    } else if (**in == '+') {
    	(*in)++;
    }
    if (**in < '0' || **in > '9') {
    	std::cerr << "PARSE ERROR! Unexpected char: " << **in << std::endl;
    	exit(1);
    }
    while (**in >= '0' && **in <= '9') {
    	val = val*10 + (**in - '0');
        (*in)++;
    }
    return _neg ? -val : val;
}
static inline void skipLine(char** in) {
    while(**in != 0) {
    	if (**in == '\n') {
    		(*in)++;
    		return;
    	}
        (*in)++;
    }
}
static inline bool eagerMatch(char* a, const char* b) {
	while(*a == *b) {
		a++;
		b++;
	}
	return *b == 0;
}

class ModelIterator;

class Models {
	int numVar;
	DdManager* ddMgr;
	DdNode* node;
	std::set<int> filter;
	std::vector<int> beginIt, endIt, convTable;

public:
	typedef ModelIterator iterator;
	Models() :
		numVar(0),
		ddMgr(NULL),
		node(NULL),
		filter(),
		beginIt(),
		endIt(),
		convTable() {
	}

	~Models() {
	}

	DdManager* getMgr() {
		return this->ddMgr;
	}
	DdNode* getNode() {
		return this->node;
	}
	int getNumVar() {
		return this->numVar;
	}

	void setMgr(DdManager* mgr) {
		this->ddMgr = mgr;
	}
	void setFilter(std::set<int> filter);
	void setNode(DdNode* node);
	void setTable(std::vector<int> table);

	bool isFiltered() {
		return this->filter.size() != 0;
	}

	ModelIterator begin();
	ModelIterator end();

	void setRange();
};

class ModelIterator {
	int numVar;
	DdManager*& ddMgr;
	DdNode*& node;
	std::set<int>& filter;
	std::vector<int>& begin;
	std::vector<int>& end;
	std::vector<int>& convTable;
	std::vector<int> index;

public:
	ModelIterator(DdManager*& ddMgr, DdNode*& node, std::set<int>& filter, std::vector<int>& begin, std::vector<int>& end, std::vector<int>& convTable, std::vector<int> index) :
		numVar(convTable.size()),
		ddMgr(ddMgr),
		node(node),
		filter(filter),
		begin(begin),
		end(end),
		convTable(convTable),
		index(index) {
	}
	ModelIterator(const ModelIterator& it) :
		numVar(it.convTable.size()),
		ddMgr(it.ddMgr),
		node(it.node),
		filter(it.filter),
		begin(it.begin),
		end(it.end),
		convTable(it.convTable),
		index(it.index) {
	}

	ModelIterator& operator++() {
		std::vector<int> end(this->numVar, 0);
		if(this->index == end) return *this;

		int pathLoc = 0;
		bool isTotalRev = false;
		std::vector<bool> isRev(this->numVar, false);
		DdNode* now = this->node;
		std::vector<DdNode*> path(this->numVar, NULL);

		// init: path and isRev
		while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
			int var;
			path[pathLoc] = now;
			isRev[pathLoc] = now->index > this->numVar || now->index < 0;
			isTotalRev = isRev[pathLoc] != isTotalRev;
			var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
			now = this->index[var] == -1 ? Cudd_E(now) : Cudd_T(now);
			pathLoc++;
		}
		int i = this->numVar - 1;

		while(true) { // loop1 begin
			while(true) { // loop2 begin
				while(!(this->index[i] == -2 && !isFind(this->filter, this->convTable[i]))) {
					if(this->index[i] == 2 || this->index[i] == -2) {
						this->index[i] = 0;
					}
					i--;
					if(i < 0) goto loop2out; // loop2 break;
				}
				this->index[i] = 2;
				i++;

	    		while(i < this->numVar) {
	    			if(this->index[i] == 0) {
	    				this->index[i] = !isFind(this->filter, -this->convTable[i]) ? -2 : 2;
	    			}
	    			i++;
	    		}
	    		goto loop1out; // loop1 break;
			} // loop2 end

			loop2out:;
	    	pathLoc--;
	    	while(true) {
	    		if(isRev[pathLoc]) {
	    			int var = Cudd_Not(path[pathLoc])->index - 1;
	    			if(this->index[var] == -1 && !isFind(this->filter, this->convTable[var])) {
	        			this->index[var] = 0;
	    				break;
	    			}
	    			this->index[var] = 0;
	    		} else {
	    			int var = path[pathLoc]->index - 1;
	    			if(this->index[var] == -1 && !isFind(this->filter, this->convTable[var])) {
	        			this->index[var] = 0;
	    				break;
	    			}
	    			this->index[var] = 0;
	    		}
	    		isTotalRev = isRev[pathLoc] != isTotalRev;
	    		isRev[pathLoc] = false;
	    		pathLoc--;
	    		if(pathLoc < 0) goto loop1out; // loop1 break;
	    	}
	    	now = path[pathLoc];
			if(isRev[pathLoc]) {
				this->index[Cudd_Not(now)->index - 1] = 1;
			} else {
				this->index[now->index - 1] = 1;
			}
			now = Cudd_T(now);
			pathLoc++;

	    	while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
	    		int var;
	    		path[pathLoc] = now;
	    		isRev[pathLoc] = now->index > this->numVar || now->index < 0;
	    		isTotalRev = isRev[pathLoc] != isTotalRev;
	    		var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
	    		if(!isFind(this->filter, -this->convTable[var])) {
	    			this->index[var] = -1;
	    			now = Cudd_E(now);
	    		} else {
	    			this->index[var] = 1;
	    			now = Cudd_T(now);
	    		}
	    		pathLoc++;
	    	}

	    	if((!isTotalRev && now == DD_ONE(this->ddMgr)) || (isTotalRev && now == Cudd_Not(DD_ONE(this->ddMgr)))) {
	    		int i = 0;
	    		while(i < this->numVar) {
	    			if(this->index[i] == 0) {
	    				this->index[i] = !isFind(this->filter, -this->convTable[i]) ? -2 : 2;
	    			}
	    			i++;
	    		}
	    		goto loop1out; // loop1 break;
	    	}
		} // loop1 end

		loop1out:;
		return *this;
	}
	ModelIterator operator++(int) {
		ModelIterator ret = *this;
		std::vector<int> end(this->numVar, 0);
		if(this->index == end) return *this;

		int pathLoc = 0;
		bool isTotalRev = false;
		std::vector<bool> isRev(this->numVar, false);
		DdNode* now = this->node;
		std::vector<DdNode*> path(this->numVar, NULL);

		// init: path and isRev
		while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
			int var;
			path[pathLoc] = now;
			isRev[pathLoc] = now->index > this->numVar || now->index < 0;
			isTotalRev = isRev[pathLoc] != isTotalRev;
			var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
			now = this->index[var] == -1 ? Cudd_E(now) : Cudd_T(now);
			pathLoc++;
		}
		int i = this->numVar - 1;

		while(true) { // loop1 begin
			while(true) { // loop2 begin
				while(!(this->index[i] == -2 && !isFind(this->filter, this->convTable[i]))) {
					if(this->index[i] == 2 || this->index[i] == -2) {
						this->index[i] = 0;
					}
					i--;
					if(i < 0) goto loop2out; // loop2 break;
				}
				this->index[i] = 2;
				i++;

	    		while(i < this->numVar) {
	    			if(this->index[i] == 0) {
	    				this->index[i] = !isFind(this->filter, -this->convTable[i]) ? -2 : 2;
	    			}
	    			i++;
	    		}
	    		goto loop1out; // loop1 break;
			} // loop2 end

			loop2out:;
	    	pathLoc--;
	    	while(true) {
	    		if(isRev[pathLoc]) {
	    			int var = Cudd_Not(path[pathLoc])->index - 1;
	    			if(this->index[var] == -1 && !isFind(this->filter, this->convTable[var])) {
	        			this->index[var] = 0;
	    				break;
	    			}
	    			this->index[var] = 0;
	    		} else {
	    			int var = path[pathLoc]->index - 1;
	    			if(this->index[var] == -1 && !isFind(this->filter, this->convTable[var])) {
	        			this->index[var] = 0;
	    				break;
	    			}
	    			this->index[var] = 0;
	    		}
	    		isTotalRev = isRev[pathLoc] != isTotalRev;
	    		isRev[pathLoc] = false;
	    		pathLoc--;
	    		if(pathLoc < 0) goto loop1out; // loop1 break;
	    	}
	    	now = path[pathLoc];
			if(isRev[pathLoc]) {
				this->index[Cudd_Not(now)->index - 1] = 1;
			} else {
				this->index[now->index - 1] = 1;
			}
			now = Cudd_T(now);
			pathLoc++;

	    	while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
	    		int var;
	    		path[pathLoc] = now;
	    		isRev[pathLoc] = now->index > this->numVar || now->index < 0;
	    		isTotalRev = isRev[pathLoc] != isTotalRev;
	    		var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
	    		if(!isFind(this->filter, -this->convTable[var])) {
	    			this->index[var] = -1;
	    			now = Cudd_E(now);
	    		} else {
	    			this->index[var] = 1;
	    			now = Cudd_T(now);
	    		}
	    		pathLoc++;
	    	}

	    	if((!isTotalRev && now == DD_ONE(this->ddMgr)) || (isTotalRev && now == Cudd_Not(DD_ONE(this->ddMgr)))) {
	    		int i = 0;
	    		while(i < this->numVar) {
	    			if(this->index[i] == 0) {
	    				this->index[i] = !isFind(this->filter, -this->convTable[i]) ? -2 : 2;
	    			}
	    			i++;
	    		}
	    		goto loop1out; // loop1 break;
	    	}
		} // loop1 end

		loop1out:;
		return ret;
	}
	std::vector<int> operator*() {
		std::vector<int> ret;
		for(int i = 0; i < this->numVar; i++) {
			ret.push_back(this->convTable[i] * (this->index[i] > 0 ? 1 : -1));
		}
		return ret;
	}

	bool operator==(const ModelIterator& it) {
		return this->index == it.index;
	}
	bool operator!=(const ModelIterator& it) {
		return this->index != it.index;
	}

	std::vector<int> getIndex() {
		return this->index;
	}
};

class Decomp {
	int numVar;
	uint64 bddSize;

public:
	Models models;


	Decomp() :
		numVar(0),
		models() {
	}

	uint64 getBddSize() {
		return this->bddSize;
	}
	int getNumVar() {
		return this->models.getNumVar();
	}

	void setFilter(std::set<int> filter) {
		this->models.setFilter(filter);
	}

	bool isFiltered() {
		return this->models.isFiltered();
	}

	void setNode(char* infilename, std::vector<int> table);
	mpz_class decompose(char* filename, double start, long int timeout);
	void printlnNumModel(const char* printStr);
};

#endif
