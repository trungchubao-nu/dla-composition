#include <algorithm>

#include "Decomp.h"

#include "dddmp.h"

char* readFile(std::ifstream& ifs) {
	int begin = static_cast<int>(ifs.tellg());
	ifs.seekg(0, ifs.end);
	int end = static_cast<int>(ifs.tellg());
	int size = end - begin;
	ifs.clear();
	ifs.seekg(0, ifs.beg);
	char* ret = new char[size + 1];
	ret[size] = 0;
	ifs.read(ret, size);
	return ret;
}

std::vector<int> parseIntLine(char** in) {
	std::vector<int> ret;
	while(**in != '\n') {
		ret.push_back(parseInt(in));
	}
	return ret;
}

bool isFind(std::set<int> filter, int n) {
	return std::find(filter.begin(), filter.end(), -n) != filter.end();
}

void Models::setFilter(std::set<int> filter) {
	this->filter = filter;
	this->setRange();
}

void Models::setNode(DdNode* node) {
	this->node = node;
	this->setRange();
}

void Models::setTable(std::vector<int> table) {
	this->convTable = table;
	this->numVar = table.size();
	beginIt.resize(this->numVar);
	endIt.resize(this->numVar);
}

ModelIterator Models::begin() {
	ModelIterator ret(this->ddMgr, this->node, this->filter, this->beginIt, this->endIt, this->convTable, this->beginIt);
	return ret;
}

ModelIterator Models::end() {
	ModelIterator ret(this->ddMgr, this->node, this->filter, this->beginIt, this->endIt, this->convTable, this->endIt);
	return ret;
}

void Models::setRange() {
	int pathLoc = 0;
	bool isTotalRev = false;
	std::vector<bool> isRev(this->numVar, false);
	DdNode* now = this->node;
	std::vector<DdNode*> path(this->numVar, NULL);

	for(int i = 0; i < this->numVar; i++) {
		this->beginIt[i] = 0;
		this->endIt[i] = 0;
	}

    while(true) { // begin loopB
    	while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
    		int var;
    		path[pathLoc] = now;
    		isRev[pathLoc] = now->index > this->numVar || now->index < 0;
    		isTotalRev = isRev[pathLoc] != isTotalRev;
    		var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
    		if(!isFind(this->filter, -this->convTable[var])) {
    			this->beginIt[var] = -1;
    			now = Cudd_E(now);
    		} else {
    			this->beginIt[var] = 1;
    			now = Cudd_T(now);
    		}
    		pathLoc++;
    	}

    	if((!isTotalRev && now == DD_ONE(this->ddMgr)) || (isTotalRev && now == Cudd_Not(DD_ONE(this->ddMgr)))) {
    		int i = 0;
    		while(i < this->numVar) {
    			if(this->beginIt[i] == 0) {
    				this->beginIt[i] = !isFind(this->filter, -this->convTable[i]) ? -2 : 2;
    			}
    			i++;
    		}
    		goto loopBout;
    	}

    	pathLoc--;
    	while(true) {
    		if(isRev[pathLoc]) {
    			int var = Cudd_Not(path[pathLoc])->index - 1;
    			if(this->beginIt[var] == -1 && !isFind(this->filter, this->convTable[var])) {
        			this->beginIt[var] = 0;
    				break;
    			}
    			this->beginIt[var] = 0;
    		} else {
    			int var = path[pathLoc]->index - 1;
    			if(this->beginIt[var] == -1 && !isFind(this->filter, this->convTable[var])) {
        			this->beginIt[var] = 0;
    				break;
    			}
    			this->beginIt[var] = 0;
    		}
    		isTotalRev = isRev[pathLoc] != isTotalRev;
    		isRev[pathLoc] = false;
    		pathLoc--;
    		if(pathLoc < 0) goto loopBout; // loopB break
    	}
    	now = path[pathLoc];
		if(isRev[pathLoc]) {
			this->beginIt[Cudd_Not(now)->index - 1] = 1;
		} else {
			this->beginIt[now->index - 1] = 1;
		}
		now = Cudd_T(now);
		pathLoc++;
    } // end loopB
    loopBout:;

    pathLoc = 0;
    now = this->node;
    isTotalRev = false;

    while(true) { // begin loopE
    	while(!(now == DD_ONE(this->ddMgr) || now == Cudd_Not(DD_ONE(this->ddMgr)))) {
    		int var;
    		path[pathLoc] = now;
    		isRev[pathLoc] = now->index > this->numVar || now->index < 0;
    		isTotalRev = isRev[pathLoc] != isTotalRev;
    		var = (isRev[pathLoc] ? Cudd_Not(now)->index : now->index) - 1;
    		if(!isFind(this->filter, this->convTable[var])) {
    			this->endIt[var] = 1;
    			now = Cudd_T(now);
    		} else {
    			this->endIt[var] = -1;
    			now = Cudd_E(now);
    		}
    		pathLoc++;
    	}

    	if((!isTotalRev && now == DD_ONE(this->ddMgr)) || (isTotalRev && now == Cudd_Not(DD_ONE(this->ddMgr)))) {
    		int i = 0;
    		while(i < this->numVar) {
    			if(this->endIt[i] == 0) {
    				this->endIt[i] = !isFind(this->filter, this->convTable[i]) ? 2 : -2;
    			}
    			i++;
    		}
    		goto loopEout;
    	}

    	pathLoc--;
    	while(true) {
    		if(isRev[pathLoc]) {
    			int var = Cudd_Not(path[pathLoc])->index - 1;
    			if(this->endIt[var] == 1 && !isFind(this->filter, -this->convTable[var])) {
        			this->endIt[var] = 0;
    				break;
    			}
    			this->endIt[var] = 0;
    		} else {
    			int var = path[pathLoc]->index - 1;
    			if(this->endIt[var] == 1 && !isFind(this->filter, -this->convTable[var])) {
        			this->endIt[var] = 0;
    				break;
    			}
    			this->endIt[var] = 0;
    		}
    		isTotalRev = isRev[pathLoc] != isTotalRev;
    		isRev[pathLoc] = false;
    		pathLoc--;
    		if(pathLoc < 0) {
    			for(int i = 0; i < this->numVar; i++) {
    				endIt[i] = 0;
     			}
    			return;
    		}
    	}
    	now = path[pathLoc];
		if(isRev[pathLoc]) {
			this->endIt[Cudd_Not(now)->index - 1] = -1;
		} else {
			this->endIt[now->index - 1] = -1;
		}
		now = Cudd_E(now);
		pathLoc++;
    } // end loopE
    loopEout:;

    ModelIterator tmp(this->ddMgr, this->node, this->filter, this->beginIt, this->endIt, this->convTable, this->endIt);
    tmp++;
    endIt = tmp.getIndex();
}

void Decomp::setNode(char* infilename, std::vector<int> table) {
	DdManager* mgr = NULL;
	DdNode* node = NULL;
	mgr = Cudd_Init(table.size(), 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
	node = Dddmp_cuddBddLoad(mgr, DDDMP_VAR_MATCHIDS, NULL, NULL, NULL, DDDMP_MODE_BINARY, infilename, NULL);

	this->models.setTable(table);
	this->models.setMgr(mgr);
    this->models.setNode(node);
    this->bddSize = (uint64)Cudd_DagSize(node);
}

mpz_class Decomp::decompose(char* filename, double start = 0, long int timeout = -1) { // timeout added by Trung Chub Bao 14/4/2019
	std::ofstream ofs(filename);
	mpz_class ret = 0;

	if(!ofs) {
		for(Models::iterator it = this->models.begin(); it != this->models.end(); it++) {
			ret++;
            if ((timeout > -1) && ((cpuTime() - start)*1000 > timeout)) return ret;
		}
	} else {
		for(Models::iterator it = this->models.begin(); it != this->models.end(); it++) {
			for(int i = 0; i < (*it).size(); i++) {
				ofs << (*it)[i] << " ";
			}
			ofs << "0\n";
			ret++;
            if ((timeout > -1) && ((cpuTime() - start)*1000 > timeout)) {
                ofs.close();
                return ret;
            }
		}
		ofs.close();
	}

	return ret;
}

void Decomp::printlnNumModel(const char* printStr) {
	int digits = 0;

	printf("%s", printStr);
	DdApaNumber numModel = Cudd_ApaCountMinterm(this->models.getMgr(), this->models.getNode(), this->models.getNumVar(), &digits);
	Cudd_ApaPrintDecimal(stdout, digits, numModel);
	printf("\n");
	fflush(stdout);
}
