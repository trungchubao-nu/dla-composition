#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* num[] = {"1","2","3"};

int main() {
char command[3][300];
int i,j;

for (j=0; j<3; j++) {
	strcpy(command[j], "python tools/maxcount-master/maxcount.py --scalmc tools/maxcount-master/scalmc sample_input/option3/population_count_16bits/population_count_3_");
	strcat(command[j], num[j]);
	strcat(command[j], "_maxcount.dimacs 2");
}

#pragma omp parallel for num_threads(3)
	for (i=0; i<3; i++) {
		system(command[i]);
	}
	return 0;
}