#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
char command[2][300];
int i, j;

strcpy(command[0], "tools/cnf2bdd/pc2bdd_static benchmark/population_count16_option2/population_count16_2_1_opt.dimacs benchmark/population_count16_option2/population_count16_2_1_parallel.bdd");
strcpy(command[1], "tools/cnf2bdd/pc2bdd_static benchmark/population_count16_option2/population_count16_2_2_opt.dimacs benchmark/population_count16_option2/population_count16_2_2_parallel.bdd");

#pragma omp parallel for num_threads(2)
	for (i=0; i<2; i++) {
		system(command[i]);
	}
	return 0;
}
