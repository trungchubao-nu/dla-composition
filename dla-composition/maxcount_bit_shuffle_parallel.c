#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* num[] = {"1","2"};

int main() {
char command[2][300];
int i,j;

for (j=0; j<2; j++) {
	strcpy(command[j], "python tools/maxcount-master/maxcount.py --scalmc tools/maxcount-master/scalmc sample_input/option3/bit_shuffle16/bit_shuffle16_2_");
	strcat(command[j], num[j]);
	strcat(command[j], "_maxcount.dimacs 2");
}

#pragma omp parallel for num_threads(2)
	for (i=0; i<2; i++) {
		system(command[i]);
	}
	return 0;
}