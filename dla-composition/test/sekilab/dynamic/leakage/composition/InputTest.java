package sekilab.dynamic.leakage.composition;

import static org.junit.Assert.*;

import org.junit.Test;

public class InputTest {

	@Test
	public void testInput() {
		Input ip = new Input();
		assertEquals("0", ip.option);
		assertEquals("", ip.input_file);
	}

	@Test
	public void testParse() {
		Input ip = new Input();
		String args[] = {"-option", "1", "-input", "test.txt"};
		ip.parse(args);
		assertEquals("1", ip.option);
		assertEquals("test.txt", ip.input_file);
	}
}
