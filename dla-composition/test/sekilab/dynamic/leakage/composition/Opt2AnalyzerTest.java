package sekilab.dynamic.leakage.composition;

import static org.junit.Assert.*;

import org.junit.Test;

public class Opt2AnalyzerTest {

	@Test
	public void testOpt2Analyzer() {
		Opt2Analyzer analyzer = new Opt2Analyzer("input file");
		assertEquals(analyzer.input_file, "input file");
	}

	@Test
	public void testParse() {
		Opt2Analyzer analyzer = new Opt2Analyzer("sample_input/sample_input_option2.txt");
		analyzer.parse();
		assertEquals(analyzer.num_blocks, 2);
		assertEquals(analyzer.block_list.get(0).cnf_file, "sample_input/option2/bit_shuffle16_2_1_opt.dimacs");
		assertEquals(analyzer.block_list.get(1).cnf_file, "sample_input/option2/bit_shuffle16_2_2_opt.dimacs");
		assertEquals(analyzer.block_list.get(1).output_list.size(), 16);
		assertEquals(analyzer.block_list.get(1).input_list.size(), 16);
		assertEquals(analyzer.input_type, "[BDD]");
	}
}
