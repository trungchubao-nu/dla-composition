package sekilab.dynamic.leakage.composition;

import static org.junit.Assert.*;

import org.junit.Test;

public class Opt1AnalyzerTest {

	@Test
	public void testOpt1Analyzer() {
		Opt1Analyzer analyzer = new Opt1Analyzer("filename");
		assertEquals(analyzer.input_file, "filename");
	}

	@Test
	public void testParse() {
		Opt1Analyzer analyzer = new Opt1Analyzer("test/sample_input_option1.txt");
		analyzer.parse();
		assertEquals(analyzer.cnf_file, "sample_input/option1/dining6.dimacs");
		assertEquals(analyzer.bdd_file, "sample_input/option1/dining6.dimacs.bdd");
		assertEquals(analyzer.filter_file, "sample_input/option1/dining6.dimacs.filter");
		assertEquals(analyzer.proj_file, "sample_input/option1/dining6.dimacs.proj");
		assertEquals(analyzer.result_file, "sample_input/option1/dining6.dimacs.result");
		
		assertEquals(analyzer.input_list.size(), 32);
		assertEquals((Integer) analyzer.input_list.get(0), (Integer) 41);
		assertEquals(analyzer.output_list.size(), 32);
		assertEquals((Integer) analyzer.output_list.get(0), (Integer) 3243);
	}
	
	@Test
	public void testRun() {
		Opt1Analyzer analyzer = new Opt1Analyzer("test/sample_input_option1.txt");
		analyzer.run();
		assertEquals((Integer) analyzer.model_count, (Integer) 6);
	}
}
