int main(void){
	unsigned int S, Output, N, m;
	N = 32;
	Output = 0;
	for (int i=0; i<N; i++) {
		m = 1 << (31-i);
		if (Output + m <= S) Output += m;
	}
	assert(0);
	return Output;
}
