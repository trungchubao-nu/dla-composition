#include <math.h>

int main (void) {

unsigned char x ,y; // secret inputs
unsigned char newx, newy; // observable outputs
__CPROVER_assume (x>=0 && x<125); // range limit
__CPROVER_assume (y>=0 && y<125);

newx = 0;
newy = 0;

double center = (double) 125/2.0;
double radius = center;
double degrees = (double) (3.141593*720.0/180.0);
double deltay = (double) (y - center);
double deltax = (double) (x - center);
double distance = deltax * deltax + deltay * deltay ;

if (distance < radius * radius) {
	double factor =1.0 - sqrt((double) distance)/radius;
	double d = (double) (degrees * factor * factor);
	double sine = sin(d);
	double cosine = cos(d);

	newx = (cosine * deltax - sine * deltay) + center;
	newy = (sine * deltax + cosine * deltay) + center;
} else {
	newx = x;
	newy = y;
}

assert(0);
return 0;
}
