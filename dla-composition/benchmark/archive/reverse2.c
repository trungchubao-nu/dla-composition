#include <stdio.h>

typedef unsigned char byte;
unsigned int reverse2 (unsigned int public, unsigned int secret)
{
	unsigned long mask = 0x22110UL;

	byte b1 = secret & 0xFF;
	byte b2 = (secret >> 8 ) & 0xFF;
	byte b3 = (secret >> 16) & 0xFF;
	byte b4 = (secret >> 24) & 0xFF;

	byte t;
	t = ((b1 * 0x0802UL & mask) | (b1 * 0x8020UL & 0x88440UL)) * 0x10101UL >> 16;
	b1 = ((b4 * 0x0802UL & mask) | (b4 * 0x8020UL & 0x88440UL)) * 0x10101UL >> 16;
	b4 = t;

	t = ((b2 * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32;
	b2 = ((b3 * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32;
	b3 = t;

	unsigned int res = b4;
	res = (res << 8) | b3;
	res = (res << 8) | b2;
	res = (res << 8) | b1;

	res = ((res >> 1) & public) | ((res & public) << 1);
	res = ((res >> 2) & 0x33333333) | ((res & 0x33333333) << 2);
	res = ((res >> 4) & 0x0F0F0F0F) | ((res & 0x0F0F0F0F) << 4);
	res = ((res >> 8) & 0x00FF00FF) | ((res & 0x00FF00FF) << 8);
	res = (res >> 16) | (res << 16);

	return res;
}

void main()
{
	unsigned int public = 0x5555;
	unsigned int secret = 15;
	unsigned int rev = reverse2(public, secret);

	printf("%X\n", rev);
}
