#include<stdlib.h>

typedef char g_type;

g_type nondet_int();

int main(void)
{
	g_type s, s0;
	g_type o;
	g_type count;

	s0 = s;
	s = (s & 0x55) + ((s >> 1) & 0x55);
	s = (s & 0x33) + ((s >> 2) & 0x33);
	count = (s + (s>>4)) & 0xf;
	count = count % 9;
	
	unsigned short int bit_arr[8];
	unsigned short int indices[8];
	
	// initialize
	for (unsigned int i=0; i<8; i++)
	{
	    bit_arr[i] = 0;
	    indices[i] = i;
	}
		
	// shuffle
	for (unsigned int i=8; i>8-count; i--)
	{
	    unsigned int j = nondet_int() % i;
	    bit_arr[indices[j]] = 1;
	    
	    // swap
	    unsigned int temp = indices[j];
	    indices[j] = indices[i-1];
	    indices[i-1] = temp;
	}
		
	// generate result
    o = 0;

	for (unsigned int i=0; i<8; i++)
	{
	    if (bit_arr[i] == 1) o += 1 << (8 - i - 1);
	}

	assert(0);		
	return 0;
}