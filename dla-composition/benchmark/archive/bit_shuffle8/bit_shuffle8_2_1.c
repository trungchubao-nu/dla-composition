#include<stdlib.h>

typedef char g_type;

g_type nondet_int();

int main(void)
{
	g_type s, s0;
	g_type count;

	s0 = s;
	s = (s & 0x55) + ((s >> 1) & 0x55);
	s = (s & 0x33) + ((s >> 2) & 0x33);
	count = (s + (s>>4)) & 0xf;
	count = count % 9;

	assert(0);		
	return 0;
}