#include<stdlib.h>

typedef unsigned int g_type;

g_type nondet_int();

int main(void)
{
	g_type s, s0;
	g_type o;
	g_type count;

	s0 = s;
	s = (s & 0x55555555) + ((s >> 1) & 0x55555555);
	s = (s & 0x33333333) + ((s >> 2) & 0x33333333);
	s = (s & 0x0f0f0f0f) + ((s >> 4) & 0x0f0f0f0f);
	s = (s & 0x00ff00ff) + ((s >> 8) & 0x00ff00ff);
	count = (s + (s>>16)) & 0xffff;
	count = count % 33;

	unsigned short int bit_arr[32];
	unsigned int indices[32];
	
	// initialize
	for (unsigned int i=0; i<32; i++)
	{
	    bit_arr[i] = 0;
	    indices[i] = i;
	}
	
	// shuffle
	for (unsigned int i=32; i>32-count; i--)
	{
	    unsigned int j = nondet_int() % i;
	    bit_arr[indices[j]] = 1;
	    
	    // swap
	    unsigned int temp = indices[j];
	    indices[j] = indices[i-1];
	    indices[i-1] = temp;
	}
		
	// generate result
    o = 0;

	for (unsigned int i=0; i<32; i++)
	{
	    if (bit_arr[i] == 1) o += 1 << (bit_length - i - 1);
	}
		
	assert(0);
	return 0;
}