/*
  benchmark of option 2 for 2 sub-programs
  sub-program1: 
   + loop first 16 times of the original program.
   + input: S
   + output: S1, Output1

  sub-program2: 
   + loop last 16 times of the original program.
   + input: S1, Output1
   + output: Output
*/

// Sub-program2
int main(void){
	unsigned int S, S1, Output1, Output, N, m;
	N = 32;
	Output = Output1;
	S = S1;
	for (int i=16; i<N; i++) {
		m = 1 << (31-i);
		if (Output + m <= S) Output += m;
	}
	assert(0);
	return Output;
}
