/*
  benchmark of option 2 for 3 sub-programs
  sub-program1: 
   + loop first 11 times of the original program.
   + input: S
   + output: S1, Output1

  sub-program2: 
   + loop next 11 times of the original program.
   + input: S1, Output1
   + output: S2, Output2

  sub-program3:
   + loop last 10 times of the original program.
   + input: S2, Output2
   + output: Output

*/

// Sub-program2
int main(void){
	unsigned int S, S1, S2, Output, Output1, Output2, N, m;
	N = 22;
  S = S1;
	Output = Output1;
	for (int i=11; i<N; i++) {
		m = 1 << (31-i);
		if (Output + m <= S) Output += m;
	}
	S2 = S;
	Output2 = Output;
	assert(0);
	return Output;
}
