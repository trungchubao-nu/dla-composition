/*
  benchmark of option 2 for 3 sub-programs
  sub-program1: 
   + loop first 11 times of the original program.
   + input: S
   + output: S1, Output1

  sub-program2: 
   + loop next 11 times of the original program.
   + input: S1, Output1
   + output: S2, Output2

  sub-program3:
   + loop last 10 times of the original program.
   + input: S2, Output2
   + output: Output

*/

// Sub-program3
int main(void){
	unsigned int S, S2, Output, Output2, N, m;
	N = 32;
  S = S2;
	Output = Output2;
	for (int i=22; i<N; i++) {
		m = 1 << (31-i);
		if (Output + m <= S) Output += m;
	}
	assert(0);
	return Output;
}
