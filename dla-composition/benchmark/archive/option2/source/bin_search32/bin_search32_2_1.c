/*
  benchmark of option 2 for 2 sub-programs
  sub-program1: 
   + loop first 16 times of the original program.
   + input: S
   + output: S1, Output1

  sub-program2: 
   + loop last 16 times of the original program.
   + input: S1, Output1
   + output: Output
*/

// Sub-program1
int main(void){
	unsigned int S, S1, Output, Output1, N, m;
	N = 16;
	Output = 0;
	for (int i=0; i<N; i++) {
		m = 1 << (31-i);
		if (Output + m <= S) Output += m;
	}
	S1 = S;
	Output1 = Output;
	assert(0);
	return Output;
}
