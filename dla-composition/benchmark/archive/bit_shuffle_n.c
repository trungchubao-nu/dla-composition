#include<stdlib.h>

typedef unsigned short int g_type;

g_type nondet_int();

g_type population_count(g_type N)
{
	g_type result;
	N = (N & 0x55555555) + ((N >> 1) & 0x55555555);
	N = (N & 0x33333333) + ((N >> 2) & 0x33333333);
	N = (N & 0x0f0f0f0f) + ((N >> 4) & 0x0f0f0f0f);
	N = (N & 0x00ff00ff) + ((N >> 8) & 0x00ff00ff);
	result = (N + (N>>16)) & 0xffff;
}

g_type* shuffle_bit(g_type num_bit_one, unsigned int length)
{
	g_type* res;
	unsigned short int* bit_arr;
	unsigned int* indices;
	unsigned int bit_length = length * sizeof(g_type) * 8; // 1byte = 8bits

    // allocate memory
	bit_arr = malloc(bit_length * sizeof(unsigned short int));
	res = malloc(length * sizeof(g_type));
	indices = malloc(bit_length * sizeof(unsigned int));
	
	// initialize
	for (unsigned int i=0; i<bit_length; i++)
	{
	    bit_arr[i] = 0;
	    indices[i] = i;
	}
	
	// shuffle
	for (unsigned int i=bit_length-1; i>=bit_length-num_bit_one; i--)
	{
	    unsigned int j = nondet_int() % (i+1);
	    bit_arr[indices[j]] = 1;
	    
	    // swap
	    unsigned int temp = indices[j];
	    indices[j] = indices[i];
	    indices[j] = temp;
	}
	
	/*
	for (unsigned int i=0; i<bit_length; i++) printf("%d", bit_arr[i]);
	printf("\n");
	*/
	
	// generate result
	for (unsigned int i=0; i<length; i++)
	{
	    res[i] = 0;
	    unsigned int start = i*8*sizeof(g_type);
	    unsigned int end = (i+1)*8*sizeof(g_type);
	    
	    for (unsigned int j=start; j<end; j++)
	    {
	        if (bit_arr[j] == 1) res[i] += 1 << (end - j - 1);
	    }
	}
	
	free(bit_arr);
	free(indices);
	
	return res;
}

int main(void)
{
	unsigned int N = 1;
	g_type s[N];
	g_type *o;
	g_type count = 0;

	// count number of bit ones
	for (unsigned int i=0; i<N; i++)
	{
		count += population_count(s[i]);
	}

    //printf("%d\n", count);

	// allocate memory
	o = malloc(N * sizeof(g_type));

	// shuffle
	o = shuffle_bit(count, N);

	/*
	for (unsigned int i=0; i<N; i++)
	{
	    printf("%d: %d\n", o[i], population_count(o[i]));
	}
	*/

	assert(0);
	free(o);
	return 0;
}