#include <stdio.h>

unsigned int reverse(unsigned int public, unsigned int secret)
{
    unsigned int res = secret;
    res = ((res >> 1) & public) | ((res & public) << 1);
    res = ((res >> 2) & 0x33333333) | ((res & 0x33333333) << 2);
    res = ((res >> 4) & 0x0F0F0F0F) | ((res & 0x0F0F0F0F) << 4) ;
    res = ((res >> 8) & 0x00FF00FF | ((res & 0x00FF00FF) << 8)) ;
    res = (res >> 16) | (res << 16);
    return res;
}

int main()
{
    unsigned int public = 0x55555555;
    unsigned int secret = 15;
    unsigned int ret = reverse(public, secret);
    
    printf("%x\n", ret);
    printf("%d\n", sizeof(int));
}