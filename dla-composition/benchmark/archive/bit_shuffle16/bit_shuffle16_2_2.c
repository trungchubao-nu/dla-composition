#include<stdlib.h>

typedef unsigned short int g_type;

g_type nondet_int();

int main(void)
{
	g_type o;
	g_type count;

	count = count % 6;
	
	unsigned short int bit_arr[16];
	unsigned short int indices[16];
	
	// initialize
	for (unsigned int i=0; i<16; i++)
	{
	    bit_arr[i] = 0;
	    indices[i] = i;
	}
		
	// shuffle
	for (unsigned int i=16; i>16-count; i--)
	{
	    unsigned int j = nondet_int() % i;
	    bit_arr[indices[j]] = 1;
	    
	    // swap
	    unsigned int temp = indices[j];
	    indices[j] = indices[i-1];
	    indices[i-1] = temp;
	}
		
	// generate result
    o = 0;

	for (unsigned int i=0; i<16; i++)
	{
	    if (bit_arr[i] == 1) o += 1 << (16 - i - 1);
	}

	assert(0);		
	return 0;
}