#include<stdlib.h>

typedef unsigned short int g_type;

g_type nondet_int();

int main(void)
{
	g_type s, s0;
	g_type count;

	s0 = s;
	s = (s & 0x5555) + ((s >> 1) & 0x5555);
	s = (s & 0x3333) + ((s >> 2) & 0x3333);
	s = (s & 0x0f0f) + ((s >> 4) & 0x0f0f);
	count = (s + (s>>8)) & 0xff;
	count = count % 6;

	assert(0);		
	return 0;
}