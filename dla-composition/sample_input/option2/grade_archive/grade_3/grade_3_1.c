typedef unsigned int size_t;

size_t nondet_int();

int main(void)
{	
	// S is the number of the student
	size_t S = 4;
	// G is the number of grade, from 0 to G-1
	size_t G = 5;
	// n is the number of random numbers generated
	size_t n;

	// these are the random numbers; each one is shared between two students
	size_t numbers[S];

	// there are S secret votes, each one with G possible values:
	size_t h[S]; // h[i] can only take value from 0 to G-1

	// calculating n
	n = ((G-1)*S)+1;
	
	// counter
	size_t c = 0;

	// generate the random numbers
	for (c = 0; c < S; c++) {
		h[c] = nondet_int() % G;
	}

	// generate the random numbers
	for (c = 0; c < S; c++) {
		numbers[c] = nondet_int() % n;
	}

	assert(0);
	return 0;
}
