typedef unsigned int size_t;

size_t nondet_int();

int main(void)
{	
	// S is the number of the student
	size_t S = 4;
	// G is the number of grade, from 0 to G-1
	size_t G = 5;
	// n is the number of random numbers generated
	size_t n;
	// this is the sum that will be printed
	size_t output;

	// this is an internal counter for the sum
	size_t sum = 0;

	// these are the public announcements of each student
	size_t announcements[S];

	// calculating n
	n = ((G-1)*S)+1;
	
	// counter
	size_t c = 0;

	//computing the sum, producing the output and terminating
	for (c = 0; c < S; c++) {
  		sum += announcements[c];
	}
	output = sum % n;

	assert(0);
	return 0;
}
