package sekilab.dynamic.leakage.composition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Opt2Analyzer extends Analyzer {

	/*
	 * ------------- INPUT FILE -----------
	 * [BDD]|[d-DNNF]
	 * t
	 * <CNF file name 1>
	 * -i v1 v2 v3 ... vn
	 * -o u1 u2 u3 ... um
	 * <CNF file name 2>
	 * -i v1 v2 v3 ... vn
	 * -o u1 u2 u3 ... um
	 * ...
	 * ...
	 * ...
	 * <CNF file name t>
	 * -i v1 v2 v3 ... vn
	 * -o u1 -u2 u3 ... um
	 * ------------------------------------
	 * 
	 * */

	// Variable members
	public String input_file;
	public ArrayList<InputBlock> block_list;
	public int num_blocks;
	public String input_type; //"[BDD]" or "d-DNNF"

	// Method members
	public Opt2Analyzer(String filename) {
		input_file = filename;
		block_list = new ArrayList<InputBlock>();
		num_blocks = 0;
	}

	public String parse() {
		// check if file exists
		File f = new File(input_file);
		if (!(f.exists()) || f.isDirectory()) return "input file does not exist";
		
		String st;
		// read file
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			
			// [BDD] or [d-DNNF]
			st = br.readLine();
			input_type = st;

			// number of sub-programs
			st = br.readLine();
			try {
				num_blocks = Integer.parseInt(st);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			InputBlock current_block = new InputBlock();
			while ((st = br.readLine()) != null) {
				String prefix = st.substring(0, 2);
				
				if (!prefix.equals("-i") && !prefix.equals("-o")) {
					// cnf_file -> beginning of a new block
					if (current_block.cnf_file != "") {
						block_list.add(current_block);
						current_block = new InputBlock();
					}
					current_block.cnf_file = st;
					current_block.cnf_for_ddnnf_file = st.replace("_opt.dimacs", "_ddnnf.dimacs");
					if (input_type.equals("[BDD]")) {
						current_block.bdd_file = st.replace("_opt.dimacs", ".bdd");
						current_block.filter_file = st.replace("_opt.dimacs", ".filter");
						current_block.result_file = st.replace("_opt.dimacs", "_bdd.result");						
					} else { // [d-DNNF]
						current_block.ddnnf_file = st.replace("_opt.dimacs", ".ddnnf");
						current_block.filter_file = st.replace("_opt.dimacs", ".filter");
						current_block.result_file = st.replace("_opt.dimacs", "_ddnnf.result");
					}
				} else if (prefix.equals("-i")) {
					// input
					st = st.substring(3, st.length());
					st = st.trim().replaceAll(" +", " ");
					String inp_vars[] = st.split(" ");
					
					for (int i=0; i<inp_vars.length; i++) current_block.input_list.add(inp_vars[i]);
				} else if (prefix.equals("-o")) {
					// output
					st = st.substring(3, st.length());
					st = st.trim().replaceAll(" +",  " ");
					String out_vars[] = st.split(" ");
					
					for (int i=0; i<out_vars.length; i++) current_block.output_list.add(out_vars[i]);
				}
			}
			
			if (current_block.cnf_file != "") block_list.add(current_block);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public void gen_bdd_for_block(InputBlock block) {
		// command
		String cmd = "tools/cnf2bdd/pc2bdd_static " + block.cnf_file + " " + block.bdd_file;
		System.out.println(cmd);

		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}	
	}

	public void gen_ddnnf_for_block(InputBlock block) {
		// command
		String cmd = "tools/gpmc-dnnf-master/core/pc2ddnnf_static " + block.cnf_for_ddnnf_file + " " + block.ddnnf_file;
		System.out.println(cmd);
		
		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void gen_bdd() {
		for (int i=0; i<block_list.size(); i++) gen_bdd_for_block(block_list.get(i));
	}

	public void gen_ddnnf() {
		for (int i=0; i<block_list.size(); i++) gen_ddnnf_for_block(block_list.get(i));
	}
	
	public int count_bdd_for_block(InputBlock block, boolean printResult) {
		int count = -1;
		String cmd;
		
		// command
		if (printResult)
			cmd = "tools/cnf2bdd/decompose/bdd_decompose_static " + block.bdd_file + " " + block.cnf_file + " " + block.result_file + " -f " + block.filter_file;
		else
			cmd = "tools/cnf2bdd/decompose/bdd_decompose_static " + block.bdd_file + " " + block.cnf_file + " -f " + block.filter_file;
		
		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (line.contains("#Filtered Models")) {
					line = line.substring("Filtered Models      : ".length(), line.length());
					try {
						count = Integer.parseInt(line);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
			input.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return count;
	}

	public int count_ddnnf_for_block(InputBlock block, boolean printResult) {
		int count = -1;
		String cmd;

		// command
		if (printResult)
			cmd = "tools/gpmc-dnnf-master/decompose/ddnnf_decompose_static " + block.ddnnf_file + " " + block.result_file + " -f " + block.filter_file;
		else
			cmd = "tools/gpmc-dnnf-master/decompose/ddnnf_decompose_static " + block.ddnnf_file + " -f " + block.filter_file;

		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (line.contains("#Projected Models")) {
					line = line.substring("#Projected Models    = ".length(), line.length());
					try {
						count = Integer.parseInt(line);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
			input.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return count;
	}
	
	public ArrayList <String> read_result(InputBlock block) {
		ArrayList <String> result = new ArrayList <String>();
		File f = new File(block.result_file);
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			String st;
			
			while ((st = br.readLine()) != null) {
				result.add(st);
			}
			
			br.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return result;
	}
	
	public void write_filter(InputBlock block, ArrayList <String> outputList) {
		String strFilter = "";
		int outputSize = outputList.size();
		for (int i=0; i<outputSize; i++) {
			String tmp = outputList.get(i);
			if (tmp.contains("FALSE") || tmp.contains("TRUE")) continue;
			strFilter = strFilter + tmp + " ";			
		}
		strFilter = strFilter + "0";
		try {
			PrintWriter writer = new PrintWriter(block.filter_file, "UTF-8");
			writer.println(strFilter);
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void write_filter_buff(ArrayList <String> buff, int level) {
		InputBlock block = block_list.get(level);
		String filename = block.filter_file.replace("filter", "buffFilter");
		
		try {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			for (int i=0; i<buff.size(); i++) writer.println(buff.get(i));
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public int count() {
		int finalCount = 0;
		int count = 0;
		
		// create filter file for the last block
		InputBlock lastBlock = block_list.get(num_blocks-1);
		write_filter(lastBlock, lastBlock.output_list);
		
		// count for the last block
		if (input_type.equals("[BDD]")) {
			count_bdd_for_block(lastBlock, true);				
		} else if (input_type.equals("[d-DNNF]")) {
			count_ddnnf_for_block(lastBlock, true);
		}
		
		ArrayList <String> filterBuff = new ArrayList <String>();
		filterBuff = read_result(lastBlock);
		
		// Loop: block[n-1] to block[1]
		for (int level=num_blocks-1; level>0; level--) {
			InputBlock previousBlock = block_list.get(level);
			InputBlock currentBlock = block_list.get(level-1);

			if (level > 1) { // intermediate
				ArrayList <String> tempBuff = new ArrayList <String>();
				for (int j=0; j<filterBuff.size(); j++) {
					String st = filterBuff.get(j);
					ArrayList <String> previousInput = new ArrayList <String>(previousBlock.input_list);
					ArrayList <String> currentOutput = new ArrayList <String>(currentBlock.output_list);
					for (int k=0; k<previousInput.size(); k++) {
						if (st.contains("-" + previousInput.get(k) + " ")) {
							currentOutput.set(k, "-" + currentOutput.get(k));
						}
					}
					
					write_filter(currentBlock, currentOutput);
					if (input_type.equals("[BDD]")) {
						count_bdd_for_block(currentBlock, true);
					} else if (input_type.equals("[d-DNNF]")) {
						count_ddnnf_for_block(currentBlock, true);
					}
					ArrayList <String> tempResult = new ArrayList<String> (read_result(currentBlock));
					tempBuff.addAll(tempResult);
				}
				
				filterBuff = tempBuff;
			} else { // block[1]
				for (int j=0; j<filterBuff.size(); j++) {
					String st = filterBuff.get(j);
					ArrayList <String> previousInput = new ArrayList <String>(previousBlock.input_list);
					ArrayList <String> currentOutput = new ArrayList <String>(currentBlock.output_list);
					for (int k=0; k<previousInput.size(); k++) {
						if (st.contains("-" + previousInput.get(k) + " ")) {
							currentOutput.set(k, "-" + currentOutput.get(k));							
						}
					}
					write_filter(currentBlock, currentOutput);
					if (input_type.equals("[BDD]")) {
						count = count_bdd_for_block(currentBlock, false);						
					} else if (input_type.equals("[d-DNNF]")) {
						count = count_ddnnf_for_block(currentBlock, false);
					}
					finalCount = finalCount + count;
				}
			}
		}
		return finalCount;
	}
	
	// this function is called from main()
	public void run() {
		long start = System.currentTimeMillis();
		parse();
		long end = System.currentTimeMillis();
		System.out.println("Preprocessing time (ms): " + (end - start));
		
		if (input_type.equals("[BDD]")) {
			// Generate BDD
			start = System.currentTimeMillis();
			gen_bdd();
			end = System.currentTimeMillis();
			System.out.println("Generating BDD time (ms): " + (end - start));
			
			// Counting
			start = System.currentTimeMillis();
			System.out.println("Preimage size: " + count());
			end = System.currentTimeMillis();
			System.out.println("Counting time (ms): " + (end - start));	
		} else { //[d-DNNF]
			// Generate BDD
			start = System.currentTimeMillis();
			gen_ddnnf();
			end = System.currentTimeMillis();
			System.out.println("Generating d-DNNF time (ms): " + (end - start));
			
			// Counting
			start = System.currentTimeMillis();
			System.out.println("Preimage size: " + count());
			end = System.currentTimeMillis();
			System.out.println("Counting time (ms): " + (end - start));			
		}		
	}
}