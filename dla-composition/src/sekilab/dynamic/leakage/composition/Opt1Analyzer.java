package sekilab.dynamic.leakage.composition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Opt1Analyzer extends Analyzer {
	/*
	 * ------------- INPUT FILE -----------
	 * CNF file name
	 * -i v1 v2 v3 ... vn
	 * -o u1 u2 u3 ... um
	 * ------------------------------------
	 * 
	 * */
	
	public String input_file;
	public String cnf_file;
	public String proj_file;
	public String bdd_file;
	public String filter_file;
	public String result_file;
	public ArrayList<Integer> input_list;
	public ArrayList<Integer> output_list;
	public int model_count;
	
	public Opt1Analyzer(String filename) {
		input_file = filename;
		input_list = new ArrayList<Integer>();
		output_list = new ArrayList<Integer>();
	}
	
	// parse the input file
	public String parse() {
		File f = new File(input_file);
		if (!f.exists() || f.isDirectory()) return "input file does not exist!";

		int val;
		// read file
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			String st;
			
			// cnf file name
			st = br.readLine();
			if (st != null) {
				cnf_file = st;
				proj_file = cnf_file + ".proj";
				bdd_file = cnf_file + ".bdd";
				filter_file = cnf_file + ".filter";
				result_file = cnf_file + ".result";
			}
			
			// loop through lines
			while((st = br.readLine()) != null) {
				// remove all multiple continuous spaces
				st = st.trim().replaceAll(" +", " ");
				
				if (st.contains("-i")) {
					st = st.substring(3, st.length());
					String inp_vars[] = st.split(" ");
					for (int i=0; i<inp_vars.length; i++) {
						try {
							val = Integer.parseInt(inp_vars[i]);
						} catch (NumberFormatException e) {
							br.close();
							return "parameter parsing error!";
						} catch (NullPointerException e) {
							br.close();
							return "parameter parsing error!";
						}
						
						input_list.add(val);
					}					
					System.out.println(input_list.toString());
				} else if (st.contains("-o")) {
					st = st.substring(3, st.length());
					String out_vars[] = st.split(" ");
					for (int i=0; i<out_vars.length; i++) {
						try {
							val = Integer.parseInt(out_vars[i]);
						} catch (NumberFormatException e) {
							br.close();
							return "parameter parsing error!";
						} catch (NullPointerException e) {
							br.close();
							return "parameter parsing error!";
						}
						
						output_list.add(val);
					}					
					System.out.println(output_list.toString());

				} else {
					// erroneous
					br.close();
					return "parameter parsing error!";
				}
			}
			br.close();
		} catch (Exception e) {
			// "File not exist" has been filtered already
			// "File is directory" has been filtered already
			e.printStackTrace();
		}
		return "";
	}
	
	public void write_projection_cnf() {
		String st;
		try {
			PrintWriter writer = new PrintWriter(proj_file, "UTF-8");
			File f = new File(cnf_file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			
			String projection_line = "cr";
			for (int i=0; i<input_list.size(); i++) {
				projection_line = projection_line + " " + Integer.toString(Math.abs(input_list.get(i)));
			}
			
			for (int i=0; i<output_list.size(); i++) {
				projection_line = projection_line + " " + Integer.toString(Math.abs(output_list.get(i)));
			}
			
			// projection line
			projection_line = projection_line + " 0";
			writer.println(projection_line);
			
			// cnf all lines
			while ((st = br.readLine()) != null) {
				writer.println(st);
			}
			br.close();
			writer.close();
		} catch(Exception e) {
			System.out.print(e.getStackTrace());
		}

	}
	
	// generate bdd from cnf
	public void gen_bdd() {
		// command
		String cmd = "tools/cnf2bdd/pc2bdd_static " + proj_file + " " + bdd_file;

		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			
		}	
	}
	
	// generate filter file
	public void gen_filter() {
		try {
			PrintWriter writer = new PrintWriter(filter_file, "UTF-8");
			String line = output_list.get(0).toString();
			for (int i=1; i<output_list.size(); i++) {
				line = line + " " + output_list.get(i).toString();
			}
			
			writer.println(line);
			writer.close();
		} catch (Exception e) {
			
		}
	}
	
	// count models
	public void count() {		
		// command
		String cmd = "tools/cnf2bdd/decompose/bdd_decompose_static " + bdd_file + " " + proj_file + " " + result_file + " -f " + filter_file;

		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (line.contains("#Filtered Models")) {
					line = line.substring("Filtered Models      : ".length(), line.length());
					try {
						model_count = Integer.parseInt(line);
					} catch (Exception e) {
						
					}
				}
			}
			input.close();
		} catch (Exception e) {
			
		}
	}
	
	public void run() {
		long start = System.currentTimeMillis();
		parse();
		write_projection_cnf();
		long end = System.currentTimeMillis();
		System.out.println("Preprocessing time (ms): " + (end - start));
		
		// BDD
		start = System.currentTimeMillis();
		gen_bdd();
		end = System.currentTimeMillis();
		System.out.println("Generating BDD time (ms): " + (end - start));
		
		// counting
		start = System.currentTimeMillis();
		gen_filter();
		count();
		end = System.currentTimeMillis();
		System.out.println("Counting time (ms): " + (end - start));
		System.out.println("Model count:" + model_count);
	}
}
