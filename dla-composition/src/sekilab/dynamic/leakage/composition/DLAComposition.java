package sekilab.dynamic.leakage.composition;

public class DLAComposition {
	// -option [1|2|3] -input [input file name]
	// ******
	// [option = 1]: Exact computing without decomposition
	// <CNF file name>
	// -i v1 v2 v3 ... v_i
	// -o -u1 -u2 u3 ... u_o
	// ******
	// [option = 2]: Exact computing with sequential composition
	// n
	// --- n-1 blocks in the next format ---
	// <CNF file name>
	// -i v1 v2 v3 ... v_i
	// -o u1 u2 u3 ... u_o
	// --- one last block in the next format ---
	// <CNF file name>
	// -i v1 v2 v3 ... v_i
	// -o u1 -u2 -u3 ... u_o
	// ******
	// [option = 3]: Approximated computing with sequential composition
	// --- similar to option 2 ---

	public static void main(String[] args) {		
		Analyzer analyzer = new Analyzer();
		Input ip = new Input();
		ip.parse(args);
		
		switch (ip.option) {
		case "1":
			analyzer = new Opt1Analyzer(ip.input_file);
			break;
		case "2":
			analyzer = new Opt2Analyzer(ip.input_file);
			break;
		case "3":
			analyzer = new Opt3Analyzer(ip.input_file);
			break;
		default:
			//analyzer = new Opt1Analyzer(ip.input_file);
		}
		
		analyzer.run();
	}
}
