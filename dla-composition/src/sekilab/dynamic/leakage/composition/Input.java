package sekilab.dynamic.leakage.composition;

import org.apache.commons.cli.*;

public class Input {
	// dla-composition -option [1/2/3] -input [input file name]

	public String option;
	public String input_file;
	
	public Input() {
		option = "0";
		input_file = "";
	}

	public void parse(String[] args) {
		Options options = new Options();
		
		Option opt = new Option("option", "option", true, "choice of function");
		opt.setRequired(true);
		options.addOption(opt);
		
		Option inp_file = new Option("input", "input", true, "input file");
		inp_file.setRequired(true);
		options.addOption(inp_file);
		
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;
		
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);
			
			System.exit(1);
		}
		
		input_file = cmd.getOptionValue("input");
		option = cmd.getOptionValue("option");
	}
}
