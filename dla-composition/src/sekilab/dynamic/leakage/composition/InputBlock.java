package sekilab.dynamic.leakage.composition;

import java.util.ArrayList;

public class InputBlock {
	public String cnf_file;
	public String bdd_file;
	public String ddnnf_file;
	public String result_file;
	public String filter_file;
	public String cnf_for_maxcount_file;
	public String cnf_for_ddnnf_file;
	public ArrayList <String> input_list;
	public ArrayList <String> output_list;
	public long maxcount;
	
	public InputBlock() {
		maxcount = -1;
		cnf_file = "";
		bdd_file = "";
		ddnnf_file = "";
		result_file = "";
		filter_file = "";
		cnf_for_maxcount_file = "";
		cnf_for_ddnnf_file = "";
		input_list = new ArrayList <String> ();
		output_list = new ArrayList <String> ();
	}
}
