package sekilab.dynamic.leakage.composition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Opt3Analyzer extends Analyzer {
	/*
	 * ------------- INPUT FILE -----------
	 * [BDD]|[d-DNNF]
	 * t
	 * <CNF file name 1>
	 * -i v1 v2 v3 ... vn
	 * -o u1 u2 u3 ... um
	 * <CNF file name 2>
	 * -i v1 v2 v3 ... vn
	 * -o u1 u2 u3 ... um
	 * ...
	 * ...
	 * ...
	 * <CNF file name t>
	 * -i v1 v2 v3 ... vn
	 * -o u1 -u2 u3 ... um
	 * ------------------------------------
	 * 
	 * */

	// Variable members
	public String input_file;
	public ArrayList<InputBlock> block_list;
	public int num_blocks;
	public String input_type; //"[BDD]" or "d-DNNF"
	public int timeout_ms; // in milliseconds
	long global_start;		// for cross-method computation
	long global_end;

	// Method members
	public Opt3Analyzer(String filename) {
		input_file = filename;
		block_list = new ArrayList<InputBlock>();
		num_blocks = 0;
	}

	public String parse() {
		// check if file exists
		File f = new File(input_file);
		if (!(f.exists()) || f.isDirectory()) return "input file does not exist";

		String st;
		// read file
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			
			// [BDD] or [d-DNNF]
			st = br.readLine();
			input_type = st;

			// number of sub-programs
			st = br.readLine();
			try {
				num_blocks = Integer.parseInt(st);
			} catch (Exception e) {
				e.printStackTrace();
			}
			InputBlock current_block = new InputBlock();
			while ((st = br.readLine()) != null) {
				String prefix = st.substring(0, 2);
				if (!prefix.equals("-i") && !prefix.equals("-o") && !prefix.equals("-t")) {
					// cnf_file -> beginning of a new block
					if (current_block.cnf_file != "") {
						block_list.add(current_block);
						current_block = new InputBlock();
					}
					current_block.cnf_file = st;
					current_block.cnf_for_maxcount_file = st.replace("_opt.dimacs", "_maxcount.dimacs");
					current_block.cnf_for_ddnnf_file = st.replace("_opt.dimacs", "_ddnnf.dimacs");
					if (input_type.equals("[BDD]")) {
						current_block.bdd_file = st.replace("_opt.dimacs", ".bdd");
						current_block.filter_file = st.replace("_opt.dimacs", ".filter");
						current_block.result_file = st.replace("_opt.dimacs", "_bdd.result");						
					} else { // [d-DNNF]
						current_block.ddnnf_file = st.replace("_opt.dimacs", ".ddnnf");
						current_block.filter_file = st.replace("_opt.dimacs", ".filter");
						current_block.result_file = st.replace("_opt.dimacs", "_ddnnf.result");
					}
				} else if (prefix.equals("-i")) {
					// input
					st = st.substring(3, st.length());
					st = st.trim().replaceAll(" +", " ");
					String inp_vars[] = st.split(" ");
					
					for (int i=0; i<inp_vars.length; i++) current_block.input_list.add(inp_vars[i]);
				} else if (prefix.equals("-o")) {
					// output
					st = st.substring(3, st.length());
					st = st.trim().replaceAll(" +",  " ");
					String out_vars[] = st.split(" ");
					
					for (int i=0; i<out_vars.length; i++) current_block.output_list.add(out_vars[i]);
				} else if (prefix.equals("-t")) {
					// timeout
					st = st.substring(3, st.length());
					st = st.trim().replaceAll(" +", " ");
					
					try {
						timeout_ms = Integer.parseInt(st);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
			
			if (current_block.cnf_file != "") block_list.add(current_block);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public void gen_bdd_for_block(InputBlock block) {
		// command
		String cmd = "tools/cnf2bdd/pc2bdd_static " + block.cnf_file + " " + block.bdd_file;
		System.out.println(cmd);

		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}	
	}
	
	public void gen_ddnnf_for_block(InputBlock block) {
		// command
		String cmd = "tools/gpmc-dnnf-master/core/pc2ddnnf_static " + block.cnf_for_ddnnf_file + " " + block.ddnnf_file;
		System.out.println(cmd);
		
		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void gen_bdd() {
		for (int i=0; i<block_list.size(); i++) gen_bdd_for_block(block_list.get(i));
	}
	
	public void gen_ddnnf() {
		for (int i=0; i<block_list.size(); i++) gen_ddnnf_for_block(block_list.get(i));
	}
	
	public int count_bdd_for_block_with_timeout(InputBlock block, long timeout, boolean printResult) {
		int count = -1;
		
		// command
		String cmd = "tools/cnf2bdd/decompose/bdd_decompose_static " + block.bdd_file + " " + block.cnf_file;
		if (printResult) {
			cmd = cmd + " " + block.result_file;
		}
		
		cmd = cmd + " -f " + block.filter_file;
		
		if (timeout > 0) {
			cmd = cmd + " -t " + timeout;
		}
		
		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (line.contains("#Filtered Models")) {
					line = line.substring("Filtered Models      : ".length(), line.length());
					try {
						count = Integer.parseInt(line);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
			input.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
			
		return count;

	}
	
	public int count_ddnnf_for_block_with_timeout(InputBlock block, long timeout, boolean printResult) {
		int count = -1;		
		
		// command
		String cmd = "tools/gpmc-dnnf-master/decompose/ddnnf_decompose_static " + block.ddnnf_file;
		
		if (printResult) {
			cmd =  cmd + " " + block.result_file;
		}
		
		cmd = cmd + " -f " + block.filter_file;
		
		if (timeout > 0) {
			cmd = cmd + " -t " + timeout;
		}
		System.out.println(cmd);
		try {
			Process p;
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				System.out.println(line);
				if (line.contains("#Projected Models")) {
					line = line.substring("#Projected Models    = ".length(), line.length());
					try {
						count = Integer.parseInt(line);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
			input.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return count;
	}

	public ArrayList <String> read_result(InputBlock block) {
		ArrayList <String> result = new ArrayList <String>();
		File f = new File(block.result_file);
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			String st;
			
			while ((st = br.readLine()) != null) {
				result.add(st);
			}
			
			br.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return result;
	}
	
	public void write_filter(InputBlock block, ArrayList <String> outputList) {
		String strFilter = "";
		int outputSize = outputList.size();
		for (int i=0; i<outputSize; i++) {
			String tmp = outputList.get(i);
			if (tmp.contains("FALSE") || tmp.contains("TRUE")) continue;
			strFilter = strFilter + tmp + " ";			
		}
		strFilter = strFilter + "0";

		try {
			PrintWriter writer = new PrintWriter(block.filter_file, "UTF-8");
			writer.println(strFilter);
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void write_filter(InputBlock block, String filter) {
		try {
			PrintWriter writer = new PrintWriter(block.filter_file, "UTF-8");
			writer.println(filter);
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public ArrayList <String> enumerate_preimage_with_timeout(int level, ArrayList <String> outputList, long timeout) {
		ArrayList <String> preimage = new ArrayList <String>();
		InputBlock block = block_list.get(level);

		write_filter(block, outputList);
		if (input_type.equals("[BDD]")) {
			count_bdd_for_block_with_timeout(block, timeout, true); // because this function is called only from level > 1			
		} else if (input_type.equals("[d-DNNF]")) {
			count_ddnnf_for_block_with_timeout(block, timeout, true);
		}

		preimage = read_result(block);
		return preimage;
	}

	
	public ArrayList <String> enumerate_preimage_with_heuristics(int level, ArrayList <String> outputList, long timeout) {
		ArrayList <String> preimage = new ArrayList <String>();
		preimage = enumerate_preimage_with_timeout(level, outputList, timeout);
		return preimage;
	}
	
	public ArrayList <String> preimage_to_output_list(int level, String preimage) {
		ArrayList <String> outputList = new ArrayList <String>(block_list.get(level-2).output_list);
		ArrayList <String> inputList = new ArrayList <String>(block_list.get(level-1).input_list);
		
		for (int i=0; i<inputList.size(); i++) {
			if (preimage.contains("-" + inputList.get(i) + " ")) {
				outputList.set(i, "-" + outputList.get(i));
			}
		}
		return outputList;
	}
	
	public long count_with_timeout() {
		int accumulate_count = 0;
		long remaining_time = 0;
		
		// initialize empty preimage sets for all n levels
		ArrayList <ArrayList <String>> preimageSet = new ArrayList <ArrayList <String>>();
		for (int i=0; i<num_blocks; i++) {
			ArrayList <String> pre = new ArrayList <String>();
			preimageSet.add(pre);
		}
		
		ArrayList <String> stack = new ArrayList <String>();
		int level = num_blocks;
		// get the output of the last block
		InputBlock lastBlock = block_list.get(num_blocks-1);
		ArrayList <String> o = lastBlock.output_list;
		String o_str = "";
		int oSize = o.size();
		for (int i=0; i<oSize; i++) {
			String tmp = o.get(i);
			if (tmp.contains("FALSE") || tmp.contains("TRUE")) continue;
			o_str = o_str + tmp + " ";			
		}
		o_str = o_str + "0";

		// Push(stack, o)
		stack.add(o_str);
		
		// Pre[n] <- enumeratePreWithHeuristics(P[n], P[n-1], o)
		remaining_time = timeout_ms - (System.currentTimeMillis() - global_start);
		if (remaining_time > 0)
			preimageSet.set(num_blocks-1, enumerate_preimage_with_heuristics(num_blocks-1, o, remaining_time));
		
		// Loop
		System.out.println("TIMEOUT: " + timeout_ms);
		while (!stack.isEmpty()) {
			if (level == 1) {
				InputBlock firstBlock = block_list.get(0);
				String stackTop = stack.get(stack.size() - 1);
				write_filter(firstBlock, stackTop);
				int count = 0;
				remaining_time = timeout_ms - (System.currentTimeMillis() - global_start);
				if (remaining_time > 0) {
					if (input_type.equals("[BDD]")) {
						count = count_bdd_for_block_with_timeout(firstBlock, timeout_ms - (System.currentTimeMillis() - global_start), false); // printResult = false
					} else if (input_type.equals("[d-DNNF]")) {
						count = count_ddnnf_for_block_with_timeout(firstBlock, timeout_ms - (System.currentTimeMillis() - global_start) ,false);
					}
					accumulate_count = accumulate_count + count;
					level = level + 1;
					stack.remove(stackTop);					
				} else {
					break;
				}
			} else {
				ArrayList <String> currentSet = preimageSet.get(level-1);
				if (currentSet.isEmpty()) { // All seletecd
					level = level + 1;
					stack.remove(stack.size()-1);
				} else {
					String v = currentSet.get(0);
					currentSet.remove(0);
					
					// from preimage of P[level] to output_list of P[level-1]
					ArrayList <String> output_list = preimage_to_output_list(level, v);
					String output_str = "";
					int outputSize = output_list.size();
					for (int i=0; i<outputSize; i++) {
						String tmpStr = output_list.get(i);
						if (tmpStr.contains("FALSE") || tmpStr.contains("TRUE")) continue;
						output_str = output_str + tmpStr + " ";			
					}
					output_str = output_str + "0";

					stack.add(output_str);
					level = level - 1;
					remaining_time = timeout_ms - (System.currentTimeMillis() - global_start);
					if (remaining_time > 0) {
						if (level > 1) preimageSet.set(level-1, enumerate_preimage_with_heuristics(level-1, output_list, timeout_ms - (System.currentTimeMillis() - global_start)));
					} else {
						break;
					}
				}
			}			
		}
		return accumulate_count;
	}
		
	// k is the number of time of self-composition. In general, greater k, more precise estimation
	public void compute_maxcounts(int k) {
		// Loop through block list
		for (int i=0; i<block_list.size(); i++) {
			InputBlock block = block_list.get(i);

			// command
			String cmd = "python tools/maxcount-master/maxcount.py --scalmc tools/maxcount-master/scalmc " + block.cnf_for_maxcount_file + " " + k;
			System.out.println(cmd);
			
			try {
				Process p;
				p = Runtime.getRuntime().exec(cmd);
				System.out.println(p.waitFor());
				String line;
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((line = input.readLine()) != null) {
					System.out.println(line);
					if (line.contains("Estimated max-count:")) {
						line = line.substring("c Estimated max-count: ".length(), line.length());
						String realPart = line.substring(0, line.indexOf(" x"));
						String expPart = line.substring(line.indexOf("x ") + 2, line.length());
						String basePart = expPart.substring(0, expPart.indexOf("^"));
						String powerPart = expPart.substring(expPart.indexOf("^") + 1, expPart.length());
						try {
							float real = Float.parseFloat(realPart);
							float base = Float.parseFloat(basePart);
							float power = Float.parseFloat(powerPart);
							long count = Math.round(real * Math.pow(base, power));
							
							block.maxcount = count;
							System.out.println("Maxcount of block " + i + ": " + count);
						} catch (Exception e) {
							System.out.println(e.getStackTrace());
						}
					}
				}
				input.close();
			} catch (Exception e) {
				System.out.println(e.getStackTrace());
			}	
		}		
	}
	
	public long compute_upper_bound_bdd() {
		long upperBound = 0;
		InputBlock lastBlock = block_list.get(block_list.size()-1);
		write_filter(lastBlock, lastBlock.output_list);
		
		upperBound = count_bdd_for_block_with_timeout(lastBlock, -1, false); // printResult = false
		for (int i=0; i<block_list.size()-1; i++) upperBound = upperBound * block_list.get(i).maxcount;
		return upperBound;
	}
	
	public long compute_upper_bound_ddnnf() {
		long upperBound = 0;
		InputBlock lastBlock = block_list.get(block_list.size()-1);
		write_filter(lastBlock, lastBlock.output_list);
		
		upperBound = count_ddnnf_for_block_with_timeout(lastBlock, -1, false);
		for (int i=0; i<block_list.size()-1; i++) upperBound = upperBound * block_list.get(i).maxcount;
		return upperBound;
	}
	
	// this function is called from main()
	public void run() {
		long start = System.currentTimeMillis();
		parse();
		long end = System.currentTimeMillis();
		System.out.println("------ Preprocessing time (ms): " + (end - start));
		
		
		if (input_type.equals("[BDD]")) {
			// Generate BDD
			start = System.currentTimeMillis();
			gen_bdd();
			end = System.currentTimeMillis();
			System.out.println("------ Generating BDD time (ms): " + (end - start));
			
			// Compute lower bound of the preimage's size
			start = System.currentTimeMillis();
			global_start = start;
			long lowerBound = count_with_timeout();
			System.out.println("Lower bound of preimage size: " + lowerBound);
			end = System.currentTimeMillis();
			System.out.println("------ Counting time for lower bound (ms): " + (end - start));			
		} else if (input_type.equals("[d-DNNF]")){ //[d-DNNF]
			// Generate d-DNNF
			start = System.currentTimeMillis();
			gen_ddnnf();
			end = System.currentTimeMillis();
			System.out.println("------ Generating d-DNNF time (ms): " + (end - start));
			
			// Compute lower bound of the preimage's size
			start = System.currentTimeMillis();
			global_start = start;
			long lowerBound = count_with_timeout();
			System.out.println("Lower bound of preimage size: " + lowerBound);
			end = System.currentTimeMillis();
			System.out.println("------ Counting time for lower bound (ms): " + (end - start));
		}
		
		// Compute upper bound of the preimage's size
		start = System.currentTimeMillis();
		compute_maxcounts(2);
		end = System.currentTimeMillis();
		System.out.println("------ Computing maxcounts for all blocks time (ms): " + (end-start));
	
		start = System.currentTimeMillis();
		if (input_type.equals("[BDD]")) {
			System.out.println("Upper bound of preimage size: " + compute_upper_bound_bdd());;
		} else { //[d-DNNF]
			System.out.println("Upper bound of preimage size: " + compute_upper_bound_ddnnf());
		}
		end = System.currentTimeMillis();
		System.out.println("------ Computing time for upper bound (ms): " + (end-start));
	}
}