#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
char command[2][300];

strcpy(command[0], "tools/gpmc-dnnf-master/core/pc2ddnnf_static benchmark/population_count16_option2/population_count16_2_1.dimacs benchmark/population_count16_option2/population_count16_2_1_parallel.ddnnf");
strcpy(command[1], "tools/gpmc-dnnf-master/core/pc2ddnnf_static benchmark/population_count16_option2/population_count16_2_2.dimacs benchmark/population_count16_option2/population_count16_2_2_parallel.ddnnf");

#pragma omp parallel for num_threads(2)
	for (i=0; i<2; i++) {
		system(command[i]);
	}
	return 0;
}
